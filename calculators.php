<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
       Calculators
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css" media="screen">
    <link rel="stylesheet" href="css/style_login.css" type="text/css" />
    <link rel="stylesheet" href="css/footer.css" type="text/css" />
    <link rel="stylesheet" href="css/style_responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/header.css" type="text/css" />
    <script src="scripts/script_responsive.js"></script>
</head>
<body>
<div id="art-main">
<nav class="art-nav clearfix">
    <ul class="art-hmenu">
        <li><a href="home.php" >Home</a></li>
        <li><a href="myprofile.php">My profile</a></li>
        <li><a href="meals.php">Meals</a></li>
        <li><a href="calculators.php" class="active">Calculators</a></li>
        <li><a href="articles.php">Articles</a></li>
        <li><a href="diet.php">Diet</a></li>
        <li><a href="about-us.php">About Us</a></li>
        
    </ul>
    </nav>

<header class="art-header clearfix">

    <div class="art-shapes">
        <h1 class="art-headline" data-left="2.6%">
    <a href="#">Nutrition</a>
</h1>
        <h2 class="art-slogan" data-left="2.45%">Calculators</h2>

        <div class="art-object0" data-left="100%"></div>

    </div>
</header>

    <div class="art-sheet clearfix">
            <div class="art-layout-wrapper clearfix">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content clearfix"><article class="art-post art-article">
                                <h2 class="art-postheader" style="text-align: center">EXAMPLE</h2>
                                                
                <div class="art-postcontent art-postcontent-0 clearfix"><div class="art-content-layout">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-0" style="width: 100%" >
        <p><img width="310" height="268" alt="" src="images/shutterstock_12539920.jpg" style="float: left; margin-right: 10px" />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pharetra, tellus sit amet congue vulputate, nisi erat iaculis nibh, vitae feugiat sapien ante eget mauris. Cras elit nisl, rhoncus nec iaculis ultricies, feugiat eget sapien. Pellentesque ac felis tellus. Aenean sollicitudin imperdiet arcu, vitae dignissim est posuere id. Duis placerat justo eu nunc interdum ultrices. Phasellus elit dolor, porttitor id consectetur sit amet, posuere id magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pharetra auctor pharetra. Nunc a sollicitudin est. Curabitur ullamcorper gravida felis, sit amet scelerisque lorem iaculis sed.</p>
        
        <p>Donec vel neque in neque porta venenatis sed sit amet lectus. Morbi tristique posuere consequat. Sed bibendum tincidunt porttitor. Maecenas ut lorem lacus. Suspendisse et sem odio, at eleifend massa. Donec ut hendrerit enim.Fusce ornare elit nisl, feugiat bibendum lorem. Vivamus pretium dictum sem vel laoreet. In fringilla pharetra purus, semper vulputate ligula cursus in. Donec at nunc nec dui laoreet porta eu eu ipsum. Sed eget lacus sit amet risus elementum dictum. Pellentesque sit amet imperdiet nunc.</p>
        
        <p>Aenean tellus mi, adipiscing sit amet laoreet eget, lobortis quis nisl. Quisque volutpat urna orci, id gravida nisi. Nullam posuere interdum est sit amet aliquam. Suspendisse malesuada metus ac enim feugiat commodo. Nullam eu consectetur nulla. Praesent faucibus condimentum quam.Sed at turpis vel ipsum adipiscing viverra eu eget nisi. Praesent neque turpis, volutpat ultrices porta sit amet, tempus ut ante. Nunc aliquet ultrices tortor, id accumsan metus porttitor eu.</p>
        
        <p>Mauris aliquet metus a leo iaculis at commodo metus dictum. Proin nulla nibh, tempor dictum luctus hendrerit, egestas quis enim. Mauris lacinia ultrices sem, at accumsan mi consequat sed. Mauris sit amet augue a leo porttitor porta et iaculis quam. Vestibulum at nunc risus, mattis egestas metus. Aliquam erat volutpat. Nullam quis lorem metus, ac vulputate dolor. Sed eu tellus dolor, quis mollis velit. Phasellus ut nibh tortor, vitae blandit est. Integer commodo nisi eget metus aliquet pharetra. In vitae tincidunt felis. Donec in dolor a libero feugiat fermentum. Pellentesque et turpis libero. Integer magna urna, rutrum pretium rhoncus at, fringilla non orci. Ut vel libero a arcu mattis laoreet.</p>
        
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed et faucibus mi. In hendrerit ligula ut diam imperdiet in fringilla diam suscipit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tortor nisl, ultricies quis tincidunt a, gravida ut ante. Curabitur non viverra libero. Aenean ornare arcu urna. Aenean a egestas orci. Maecenas tempus viverra erat, at hendrerit est blandit tincidunt. Fusce ut quam neque. Aliquam vitae eleifend arcu. Pellentesque hendrerit, erat faucibus varius commodo, nunc orci semper mi, non viverra diam massa eu arcu. Praesent at libero dui, ut fermentum urna.</p>
        
        <p>Morbi eget sem at eros fermentum pulvinar non quis risus. Etiam eu tortor vel dolor congue lobortis at sed arcu. Sed eget nibh libero, vitae imperdiet orci. Sed elit risus, sodales nec vehicula eget, pharetra non justo. Nunc fringilla porttitor tincidunt. Suspendisse hendrerit egestas dolor a mattis. Sed at elit et nunc tristique accumsan at a libero. Mauris dolor purus, consequat sed tempus in, tempus id magna. Integer commodo, leo nec porttitor congue, dui arcu semper lectus, ac dictum lacus velit sit amet mauris. Praesent et molestie dui. Vestibulum vulputate, nisl id interdum malesuada, massa leo eleifend velit, nec malesuada velit massa et magna. Aliquam auctor tortor sed lacus rhoncus fringilla. Sed gravida erat nec quam posuere fermentum. Donec et tempor erat. Quisque faucibus hendrerit erat, ac suscipit libero porta et.</p>
    </div>
    </div>
</div>
</div>
</article></div>
                    </div>
                </div>
            </div>
    </div>
<footer class="art-footer clearfix">
  <div class="art-footer-inner">
<p>Copyright © 2016, FSquad. All Rights Reserved.<br>
<br></p>
    <p class="art-page-footer">
        <span id="art-footnote-links"><a href="http://milance941.deviantart.com//" target="_blank">milance941</a> created with brackets.</span>
    </p>
  </div>
</footer>

</div>


</body></html>