<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
      About us
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css" media="screen">
    <link rel="stylesheet" href="css/style_login.css" type="text/css" />
    <link rel="stylesheet" href="css/footer.css" type="text/css" />
    <link rel="stylesheet" href="css/style_responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/header.css" type="text/css" />
    <script src="scripts/script.js"></script>
    <script src="scripts/jquery.js"></script>
    <script src="scripts/script_responsive.js"></script>
</head>
<body>
<div id="art-main">
<nav class="art-nav clearfix">
    <ul class="art-hmenu">
        <li><a href="home.php" >Home</a></li>
        <li><a href="myprofile.php">My profile</a></li>
        <li><a href="meals.php">Meals</a></li>
        <li><a href="nutrients.php">Nutrients</a></li>
        <li><a href="diet.php">Diet</a></li>
        <li><a href="articles.php">Forum</a></li>
        <li><a href="about-us.php" class="active">About Us</a></li>
        
    </ul>
    </nav>
<header class="art-header clearfix">

    <div class="art-shapes">
        <h1 class="art-headline" data-left="2.6%">
    <a href="#">User</a>
</h1>
        <h2 class="art-slogan" data-left="2.45%">Experience</h2>

        <div class="art-object0" data-left="100%"></div>

    </div>
</header>
<div class="art-sheet clearfix">
            <div class="art-layout-wrapper clearfix">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content clearfix"><article class="art-post art-article">
                                <h2 class="art-postheader">Testimonials</h2>
                                                
                <div class="art-postcontent art-postcontent-0 clearfix"><div class="art-content-layout">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-0" style="width: 100%" >
        <h2>Thank you for your help</h2>
        
        <p>I would like to thank you for the help, kind attitude, and the excellent care. I will encourage my colleagues, friends and others to visit your website.&nbsp;</p>
        
        <p>Curabitur ligula justo, tincidunt nec interdum elementum, iaculis nec lorem. Maecenas ultricies faucibus magna, vel feugiat ante vehicula quis. Integer semper quam ac ligula ultricies pretium. Donec vulputate, ipsum in ornare semper, justo lectus accumsan ante, non feugiat elit odio nec nulla. Mauris nec mi sed libero varius malesuada eget sed sem. Curabitur eu turpis sit amet leo mattis facilisis. Mauris in aliquam augue. Sed adipiscing ante nec augue vehicula ac tempus enim ullamcorper. Curabitur quis odio at dui fermentum elementum. Cras quam arcu, sagittis ac sodales nec, dignissim quis mi. Mauris laoreet placerat dui ut rhoncus.</p>
        
        <p><span style="font-style: italic;">- Denisse Warren, Internet Marketer</span></p>
        
        <h2>What an informative website</h2>
        
        <p>Good day. Yesterday I came accross your website, and, to be honest, the information was really helpful.&nbsp;</p>
        
        <p>Fusce ornare elit nisl, feugiat bibendum lorem. Vivamus pretium dictum sem vel laoreet. In fringilla pharetra purus, semper vulputate ligula cursus in. Donec at nunc nec dui laoreet porta eu eu ipsum. Sed eget lacus sit amet risus elementum dictum. Pellentesque sit amet imperdiet nunc. Aenean tellus mi, adipiscing sit amet laoreet eget, lobortis quis nisl. Quisque volutpat urna orci, id gravida nisi. Nullam posuere interdum est sit amet aliquam. Suspendisse malesuada metus ac enim feugiat commodo.</p>
        
        <p><span style="font-style: italic;">- Paul Sellings, Banker</span></p>
        
        <h2>Now I understood the importance of healthy eating</h2>
        
        <p>Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Sed et faucibus mi. In hendrerit ligula ut diam imperdiet in fringilla diam suscipit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean tortor nisl, ultricies quis tincidunt a, gravida ut ante. Curabitur non viverra libero. Aenean ornare arcu urna. Aenean a egestas orci. Maecenas tempus viverra erat, at hendrerit est blandit tincidunt. Fusce ut quam neque.</p>
        
        <p><span style="font-style: italic;">- Melissa Brown, Entrepreneur</span></p>
    </div>
    </div>
</div>
</div>
</article></div>
                    </div>
                </div>
            </div>
    </div>
<footer class="art-footer clearfix">
  <div class="art-footer-inner">
<p>Copyright © 2016, FSquad. All Rights Reserved.<br>
<br></p>
    <p class="art-page-footer">
        <span id="art-footnote-links"><a href="http://milance941.deviantart.com//" target="_blank">milance941</a> created with brackets.</span>
    </p>
  </div>
</footer>

</div>


</body></html>