<html lang="en-US">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
       My profile
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css" media="screen">
    <link rel="stylesheet" href="css/style_login.css" type="text/css" />
    <link rel="stylesheet" href="css/footer.css" type="text/css" />
    <link rel="stylesheet" href="css/style_responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/header.css" type="text/css" />
    <script src="scripts/script.js"></script>
    <script src="scripts/jquery.js"></script>
    <script src="scripts/script_responsive.js"></script>
    
</head>
<body>
<div id="art-main">
<nav class="art-nav clearfix">
    <ul class="art-hmenu">
        <li><a href="home.php" >Home</a></li>
        <li><a href="myprofile.php" class="active">My profile</a></li>
        <li><a href="meals.php" >Meals</a></li>
        <li><a href="nutrients.php">Nutrients</a></li>
        <li><a href="diet.php">Diet</a></li>
        <li><a href="articles.php">Forum</a></li>
        <li><a href="about-us.php">About Us</a></li>
       
    </ul>
    </nav>
<header class="art-header clearfix">

    <div class="art-shapes">
        <h1 class="art-headline" data-left="2.6%">
    <a href="#">My</a>
</h1>
        <h2 class="art-slogan" data-left="2.45%">profile</h2>

        <div class="art-object0" data-left="100%"></div>

    </div>
</header>

    <div class="art-sheet clearfix">
            <div class="art-layout-wrapper clearfix">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content clearfix"><article class="art-post art-article">
                                <h2 class="art-postheader" style="text-align: center">My profile</h2>
                                                
                <div class="art-postcontent art-postcontent-0 clearfix"><div class="art-content-layout">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-0" style="width: 100%" >
        <p><img width="310" height="268" alt="" src="images/profile.png" style="float: left; margin-right: 10px" />
        <h3>User name: milance941</h4> <br/>
        <h3>First name: Milan</h3><br/>
        <h3>Last name: Taskovic</h3><br/>
        <h3>Gender: male</h3><br/>
        <h3>Age: 22</h3><br/>
        <h4>Biography:</h4><br/>
        <h5>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam pharetra, tellus sit amet congue vulputate, nisi erat iaculis nibh, vitae feugiat sapien ante eget mauris. Cras elit nisl, rhoncus nec iaculis ultricies, feugiat eget sapien. Pellentesque ac felis tellus. Aenean sollicitudin imperdiet arcu, vitae dignissim est posuere id. Duis placerat justo eu nunc interdum ultrices. Phasellus elit dolor, porttitor id consectetur sit amet, posuere id magna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse pharetra auctor pharetra. Nunc a sollicitudin est. Curabitur ullamcorper gravida felis, sit amet scelerisque lorem iaculis sed.</h5>
        </p>
    </div>
    </div>
</div>
</div>
</article></div>
                    </div>
                </div>
            </div>
    </div>
    
<footer class="art-footer clearfix">
  <div class="art-footer-inner">
<p>Copyright © 2016, FSquad. All Rights Reserved.<br>
<br></p>
    <p class="art-page-footer">
        <span id="art-footnote-links"><a href="http://milance941.deviantart.com//" target="_blank">milance941</a> created with brackets.</span>
    </p>
  </div>
</footer>

</div>


</body></html>