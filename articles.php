<!DOCTYPE html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
      Articles
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="style.css" media="screen">
    <link rel="stylesheet" href="css/style_login.css" type="text/css" />
    <link rel="stylesheet" href="css/footer.css" type="text/css" />
    <link rel="stylesheet" href="css/style_responsive.css" type="text/css" />
    <link rel="stylesheet" href="css/header.css" type="text/css" />
    <script src="scripts/script.js"></script>
    <script src="scripts/jquery.js"></script>
    <script src="scripts/script_responsive.js"></script>
</head>
<body>
<div id="art-main">
<nav class="art-nav clearfix">
    <ul class="art-hmenu">
        <li><a href="home.php" >Home</a></li>
        <li><a href="myprofile.php">My profile</a></li>
        <li><a href="meals.php">Meals</a></li>
        <li><a href="nutrients.php">Nutrients</a></li>
        <li><a href="diet.php" >Diet</a></li>
        <li><a href="articles.php" class="active">Forum</a></li>
        <li><a href="about-us.php">About Us</a></li>
        
    </ul> 
    </nav>
<header class="art-header clearfix">

    <div class="art-shapes">
        <h1 class="art-headline" data-left="2.6%">
    <a href="#">Nutritional</a>
</h1>
        <h2 class="art-slogan" data-left="2.45%">Values</h2>

        <div class="art-object0" data-left="100%"></div>

    </div>
</header>

    <div class="art-sheet clearfix">
            <div class="art-layout-wrapper clearfix">
                <div class="art-content-layout">
                    <div class="art-content-layout-row">
                        <div class="art-layout-cell art-content clearfix"><article class="art-post art-article">
                                <h2 class="art-postheader" style="text-align: center">Nutrients</h2>
                                                
                <div class="art-postcontent art-postcontent-0 clearfix"><div class="art-content-layout">
    <div class="art-content-layout-row">
    <div class="art-layout-cell layout-item-0" style="width: 100%" >
        <p><img width="100%"  alt="" src="images/tabela005.jpg"/></p>
        
        
    </div>
    </div>
</div>
</div>
</article></div>
                    </div>
                </div>
            </div>
    </div>
    
<footer class="art-footer clearfix">
  <div class="art-footer-inner">
<p>Copyright © 2016, FSquad. All Rights Reserved.<br>
<br></p>
    <p class="art-page-footer">
        <span id="art-footnote-links"><a href="http://milance941.deviantart.com//" target="_blank">milance941</a> created with brackets.</span>
    </p>
  </div>
</footer>

</div>
</body></html>