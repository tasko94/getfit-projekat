# README #
#IF YOU DON'T SPEAK SERBIAN#
**This is a application for automated meal planning and it plans meals on a weekly basis.
It is still work in progress and for now only interface is complete (more or less).
For more information use google translate (serbian-english).**

Dragi moji dobro nam dosli u nas zajednicki projekat pod nazivom GET FIT,
koji je od velikog znacaja ne samo zbog znanja koje ce mo steci vec zbog timskog rada
koji ce verujem biti od velikog znacaja u nasoj buducoj karijeri, sem ako radite kao
freelancer (mala plata khm), zato uozbiljite se jer nema puno vremena (a vreme leti).
Zivi bili !

**Rok za predaju projekta je 4 Jul !**

### Sta se sve nalazi u ovom folderu ###

* PHP stranice za sve bitne delove aplikacije

* PHP kod za prijavljivanje i registrovanje korisnika

* CSS folder sa stilovima za uredjivanje stranica
  koji su razdvojeni zbog bolje citljivosti koda.
  Jedan css fajl (style.css) je morao da bude van css direktorijuma
  zbog problema nepojavljivanja pozadine web stranice.

* Scripts folder koji sadrzi javascript datoteku za responsive dizajn stranice
  ta skripta je genericka i standardna za sve html stranice i stoga se ne sme menjati!

* Images direktoriju koji sadrzi sve slike koje web aplikacija koristi

### Kako da pokrenem aplikaciju? ###

* Kopirajte ceo projekat u wamp/www

* Pokrenite iz pregledaca unosenjem adrese localhost/getfit

* Po otvaranju index.php datoteke svaki korisnik je obavezan da se registuje da bi pristupio sajtu

* Baza podatka sa korisnickim nalozima je napravljena na phpmyadmin pod nazivom testdb

### Greske u kodu ###

* Header koji sadrzi korisnicko ime ne smanjuje font dok se smanjuje stranica (responsive design)
  Ne primecuje se dok se stranica ne smanji na velicinu smartfona

* Datoteka style.css mora da bude izvan css direktorijuma jer dolazi do 
  problema sa pozadinom web aplikacije, ne znam zasto sve ostalo radi

* Logovanje radi samo za korisnika! Za admina i nutricionistu nisam jos napravio

### Stvari koje treba da uradimo ###

* Napravimo bazu za namirnice

* Napravimo kalkulatore (sa css)

* Forum

* Admin i Nutriionista (nalozi)

* +++

### Kome da se obratim ako nesto na valja? ###

* Nas uvazeni admin Aleksa Vasiljevic koji je zaduzen za funkcionalnosti aplikacije
  kao sto su forme i kalkulatori u jeziku php. Za sve probleme sa ovim delom koda obratiti se njemu.

* Moja malenkost Milan Taskovic zaduzen za dizajn aplikacije kao i njen softverski deo na jeziku
  php, html, css, bootstrap. Za sve probleme sa ovim delom koda obratiti se na mail: milantaskovic@yahoo.com

* Zadnji ali ne i poslednji Vidoje Mijalkovic koji se jos nije opredelio za deo projekta pa njemu ostaje forum

### PS ###

* Sve sto dodate napisite u readme !